DELIMITER $$
CREATE TRIGGER pagos AFTER
INSERT ON payments
FOR EACH ROW BEGIN IF amount >= 50000 THEN
INSERT INTO cambios
SET customerNumber = NEW.customerNumber,
    amount = NEW.amount;
END IF; END$$ DELIMITER ;
