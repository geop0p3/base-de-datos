CREATE VIEW precioalto AS
SELECT *
FROM products
ORDER BY productCode,
         productName,
         buyPrice
WHERE buyPrice >
    (SELECT AVG(buyPrice)
     FROM products);
