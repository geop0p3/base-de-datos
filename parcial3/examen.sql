/* Total de ventas en 2 fechas ✓ */
DELIMITER //
CREATE PROCEDURE totalfechas() BEGIN
SELECT SUM(total) AS Total_Ventas
FROM VENTAS
WHERE fecha BETWEEN '2007/04/16' AND '2007/04/19'; END // DELIMITER ;

/* Desarrollar un procimiendo que muestre los 5 productos mas y menos vendidos por mes ✓ */
DELIMITER //
CREATE PROCEDURE productos_menos_mas() BEGIN
SELECT producto,
       VENTAS.fecha,
       SUM(VENTA_DESGLOCE.cantidad) AS Total
FROM PRODUCTOS
INNER JOIN VENTA_DESGLOCE ON PRODUCTOS.codigo_barra = VENTA_DESGLOCE.codigo
INNER JOIN VENTAS ON VENTAS.id_venta = VENTA_DESGLOCE.id_venta
WHERE fecha BETWEEN '2007/04/01' AND '2007/04/31'
GROUP BY PRODUCTOS.producto,
         VENTAS.fecha
ORDER BY Total DESC,
         Total ASC LIMIT 5; END // DELIMITER ;

/* Mostrar Cadena de equivalencia */ ✓
SELECT EQUIVALENCIA.barcode1,
       EQUIVALENCIA.barcode2,
       EQUIVALENCIA.equivalencia,
       PRODUCTOS.CODIGO_BARRA
FROM EQUIVALENCIA
INNER JOIN PRODUCTOS ON EQUIVALENCIA.barcode1 = PRODUCTOS.codigo_barra
OR EQUIVALENCIA.barcode2 = PRODUCTOS.codigo_barra

/* Mostrar productos sin existencia ✓ */
CREATE VIEW noexistencia AS
SELECT existencia,
       INVENTARIOS.codigo_barra,
       PRODUCTOS.producto
FROM INVENTARIOS
INNER JOIN PRODUCTOS ON INVENTARIOS.codigo_barra = PRODUCTOS.codigo_barra
WHERE existencia = 0
ORDER BY codigo_barra DESC;

/* Mostrar departamento con menos venta ✓ */
CREATE VIEW departamento AS
SELECT DEPARTAMENTOS.departamento,
       MIN(VENTA_DESGLOCE.cantidad) AS Cantidad_Vendida,
       SUM(VENTA_DESGLOCE.total) AS Venta
FROM PRODUCTOS
INNER JOIN VENTA_DESGLOCE ON PRODUCTOS.codigo_barra = VENTA_DESGLOCE.codigo
INNER JOIN DEPARTAMENTOS ON PRODUCTOS.departamento = DEPARTAMENTOS.id
GROUP BY DEPARTAMENTOS.departamento
ORDER BY Venta ASC;

/* Hacer Procedimiento donde muestre los productos, precio y existencia que esten
arriba del promedio de venta ✓ */
DELIMITER //
CREATE PROCEDURE superfuncion() BEGIN DECLARE Promedio_Ventas int;
SELECT AVG(VENTAS.total) INTO Promedio_Ventas
FROM VENTAS;
SELECT PRODUCTOS.producto,
       PRODUCTOS.precio,
       VENTA_DESGLOCE.codigo,
       PRODUCTOS.codigo_barra,
       INVENTARIOS.existencia,
       INVENTARIOS.codigo_barra,
       VENTAS.total
FROM PRODUCTOS
INNER JOIN INVENTARIOS ON PRODUCTOS.codigo_barra = INVENTARIOS.codigo_barra
INNER JOIN VENTA_DESGLOCE ON PRODUCTOS.codigo_barra = VENTA_DESGLOCE.codigo
INNER JOIN VENTAS ON VENTAS.id_venta = VENTA_DESGLOCE.id_venta
WHERE VENTAS.total > Promedio_Ventas
GROUP BY PRODUCTOS.codigo_barra,
         INVENTARIOS.codigo_barra,
         VENTA_DESGLOCE.codigo,
         VENTAS.id_venta; END // DELIMITER ;

